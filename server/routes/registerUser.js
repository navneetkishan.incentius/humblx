const express = require('express')
const bcrypt = require('bcryptjs')
const router = express.Router()
const registerUser = require('../models/registerUser')


router.post('/', async(req, res)=>{
    const user = new registerUser({
        firstName: req.body.data.firstName,
        lastName: req.body.data.lastName,
        email: req.body.data.email,
        password: await bcrypt.hash(req.body.data.password, 10)
    })
    
    try {
        await user.save()
        res.json({"ok": true})

    } catch (error) {
        res.send('Error :' + error)
    }
})

module.exports = router