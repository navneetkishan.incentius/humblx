const express = require("express");
const router = express.Router();
const issuesSchema = require("../models/issues");

router.get("/", async (req, res) => {
  try {
    const issues = await issuesSchema.find();
    res.send(issues);
  } catch (error) {
    res.send("Error" + error);
  }
});

router.post("/", async (req, res) => {
  const issue = new issuesSchema({
    issueName: req.body.data.issueName,
    description: req.body.data.description,
    facility: req.body.data.facility,
  });

  try {
    await issue.save();
    res.json({ ok: true });
  } catch (error) {
    res.send("Error" + error);
  }
});

router.put("/:id", async (req, res) => {
  const id = req.params.id;
  issuesSchema
    .findByIdAndUpdate(id, req.body.data, { useFindAndModify: false })
    .then((data) => {
      res.json({ ok: true });
    });
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;

  issuesSchema.findByIdAndDelete(id).then((data) => {
    res.json({ ok: true });
  });
});

module.exports = router;
