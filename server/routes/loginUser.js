require('dotenv').config()
const jwt = require('jsonwebtoken');
const express = require('express')
const bcrypt = require('bcryptjs')
const Email = require('mongoose-type-email')
const router = express.Router()
const registerUser = require('../models/registerUser')


router.post('/', async(req, res)=>{
    try {
        const email = req.body.data.email
        const password = req.body.data.password

        const user = await registerUser.findOne({email: email})

        const passwordMatch = await bcrypt.compare(password, user.password)
        
        if(passwordMatch){
            const token = jwt.sign({_id:user._id.toString()}, process.env.SECRET_KEY)
            res.json({"ok": true, "cookie": token})
        }
        else{
            res.json({"ok": false})
        }

        
    } catch (error) {
        res.send('Error' + error)
    }
})


module.exports = router