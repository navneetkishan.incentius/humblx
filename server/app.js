require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const checkAuthMiddleware = require('./middlewares/auth');

const url = 'mongodb+srv://navneet:navneet@navneet.8t5zb.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
const app = express()
const port = process.env.PORT

mongoose.connect(url, {
  useNewUrlParser:true
}).then(()=>{
  console.log('Database connected...')
}).catch((error)=>{
  console.log(error);
})


app.use(cors())
app.use(express.json())

const issueRouter = require('./routes/issues')
app.use('/issuesList', checkAuthMiddleware, issueRouter)


const registerRouter = require('./routes/registerUser')
app.use('/registerUser',registerRouter)

const loginRouter = require('./routes/loginUser')
app.use('/loginUser',loginRouter)


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})