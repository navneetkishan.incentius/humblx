const jwt = require("jsonwebtoken");

const auth = async (req, res, next) => {
  if (req.headers.authorization) {
    try {
      // this will convert the Bearer token into the array and take only token not Bearer text 
      const token = req.headers.authorization.split(" ")[1];

      let decoded;
      
      if (token) {
        decoded = jwt.verify(token, process.env.SECRET_KEY);
        req.userId = decoded?.id;
      }

      next();
    } catch (error) {
      console.log(error);
    }
  } else {
    error = {
      error: `Token required`,
      status: 401,
    };
    res.status(401).send(error); 
  }
};

module.exports = auth;