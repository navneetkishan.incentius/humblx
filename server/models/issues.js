const mongoose = require('mongoose')

const issuesSchema = new mongoose.Schema({
    issueName: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    facility: {
        type: String,
        required: true
    },
})

module.exports = mongoose.model('issues',issuesSchema)