const routes = [
  {
    path: "/", component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "/login", component: () => import("pages/LoginPage.vue") },
      { path: "/logout", component: () => import("pages/LogoutPage.vue") },
      { path: "/registerUser", component: () => import("pages//RegisterUser.vue") },
      { path: "/userProfile", component: () => import("pages/UserProfile.vue") },
      { path: "/issues", component: () => import("pages/IssuesPage.vue") }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
